# Zeph's Hammers
A simple mod that adds various hammers able to break blocks in a 3x3 pattern.

This mod uses [EzModLib](https://gitlab.com/ZephaniahNoah/ez-mod-lib).
