package com.zephaniahnoah.hammers;

import net.minecraft.item.IItemTier;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.LazyValue;

public enum Material implements IItemTier {
	
	RHODONITE  (2, 350,  5.0F, 1.5F, 10, true ), //
	JET        (1, 350,  4.2F, 1.3F, 10, true ), //
	SERPENTINE (2, 120,  5.5F, 1.8F, 10, true ), //
	BLOOD_STONE(1, 350,  4.2F, 1.4F, 10, true ), //
	CHAROITE   (1, 850,  5.3F, 1.2F, 22, true ), //
	BRONZITE   (1, 850,  4.0F, 1.5F, 22, true ), //
	COPPER     (1, 200,  4.7F, 1.5F, 10, false), //
	TIN        (1, 120,  4.2F, 1.1F, 7 , false), //
	ALUMINUM   (1, 160,  5.6F, 1.6F, 9 , false), //
	NICKEL     (2, 225,  5.8F, 1.8F, 10, false), //
	BRASS      (2, 240,  6.1F, 2.1F, 12, false), //
	BRONZE     (2, 400,  6.2F, 2.2F, 13, false), //
	LEAD       (2, 200,  6.1F, 2.1F, 8 , false), //
	SILVER     (2, 220,  6.4F, 2.3F, 16, false), //
	TITANIUM   (3, 1100, 7.1F, 2.0F, 14, false), //
	TUNGSTEN   (3, 900,  6.3F, 2.0F, 13, false), //
	PLATINUM   (3, 900,  6.5F, 2.5F, 17, false), //
	ELECTRUM   (2, 350,  5.0F, 2.4F, 25, false), //
	ADAMANTIUM (2, 430,  9.0F, 5.5F, 15, false), //
	OSMIUM     (2, 350,  6.7F, 2.5F, 8 , false), //
	STEEL      (2, 400,  7.0F, 2.5F, 10, false), //
	PALLADIUM  (3, 200,  8.5F, 3.5F, 12, false), //
	COBALT     (3, 2350, 10.0F,3.5F, 15, false), // 
	MYTHRIL    (4, 1350, 11.7F,3.5F, 20, false), //
	ETHERIUM   (2, 550,  4.7F, 3.5F, 17, false), //
	ORICHALCUM (3, 400,  8.7F, 2.5F, 30, false), //
	MAGNETITE  (2, 350,  7.7F, 2.2F, 11, false), // 
	BISMUTH    (2, 225,  6.8F, 2.2F, 22, false), //
	SILICON    (3, 230,  7.5F, 2.4F, 16, false), //
	ZINC       (2, 190,  5.0F, 1.6F, 6 , false), //
	ANTIMONY   (2, 175,  4.7F, 2.2F, 14, false), //
	SPINEL     (2, 1000, 8.3F, 3.2F, 10, true ), //
	TOURMALINE (2, 750,  9.5F, 2.8F, 10, true ), //
	TIGERS_EYE (2, 350,  6.5F, 1.9F, 22, true ), //
	PERIDOT    (2, 350,  6.1F, 2.1F, 10, true ), //
	OPAL       (1, 350,  5.8F, 2.3F, 26, true ), //
	JASPER     (2, 450,  9.7F, 2.5F, 10, true ), //
	SELENITE   (2, 210,  4.5F, 1.6F, 22, true ), //
	GARNET     (2, 350,  5.7F, 2.5F, 25, true ), //
	MOSS_AGATE (2, 750,  6.7F, 3.4F, 10, true ), //
	JADE       (1, 450,  5.1F, 1.7F, 10, true ), //
	RUBY       (2, 350,  6.4F, 2.5F, 10, true ), //
	AMETHYST   (2, 350,  5.7F, 2.5F, 10, true ), //
	SAPPHIRE   (2, 350,  6.7F, 2.4F, 10, true ), //
	QUARTZ     (2, 350,  5.8F, 1.8F, 9 , true ), //
	ZIRCON     (2, 350,  5.7F, 2.9F, 10, true ), //
	PRISMARINE (2, 350,  5.7F, 2.4F, 10, true ), //
	SUNSTONE   (2, 550,  7.7F, 2.2F, 10, true ), //
	TOPAZ      (2, 75,   6.1F, 2.1F, 10, true ), //
	OLIVINE    (3, 900,  6.4F, 2.6F, 10, true ), //
	AMBER      (1, 125,  4.5F, 3.5F, 10, true ), //
	DEUTERIUM  (3, 500,  7.2F, 3.5F, 10, true ), //
	URANIUM    (3, 850,  6.3F, 2.2F, 22, false), //
	ONYX       (2, 350,  6.7F, 2.4F, 10, true ), //
	HEDERAIUM  (3, 1000, 7.3F, 4.5F, 33, false), //
	SHROOMITE  (3, 1100, 8.3F, 3.5F, 33, false); //

	private final int level;
	private final int uses;
	private final float speed;
	private final float damage;
	private final int enchantmentValue;
	public final String metal;
	private final LazyValue<Ingredient> repairIngredient;
	public final String tag;

	private Material(int level, int uses, float speed, float damage, int enchantability, boolean gem) {
		this.level = level;
		this.uses = uses;
		this.speed = speed;
		this.damage = damage;
		this.enchantmentValue = enchantability;
		this.metal = this.name().toLowerCase();
		this.tag = "forge:" + (gem ? "gems" : "ingots") + "/" + metal;
		this.repairIngredient = new LazyValue<>(() -> {
			return Ingredient.of(ItemTags.bind(tag));
		});
	}

	public int getUses() {
		return this.uses;
	}

	public float getSpeed() {
		return this.speed;
	}

	public float getAttackDamageBonus() {
		return this.damage;
	}

	public int getLevel() {
		return this.level;
	}

	public int getEnchantmentValue() {
		return this.enchantmentValue;
	}

	public Ingredient getRepairIngredient() {
		return this.repairIngredient.get();
	}
}
