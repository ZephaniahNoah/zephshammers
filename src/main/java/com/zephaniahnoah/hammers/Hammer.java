package com.zephaniahnoah.hammers;

import java.util.Map.Entry;

import org.apache.commons.lang3.text.WordUtils;

import com.mojang.datafixers.util.Pair;
import com.zephaniahnoah.ezmodlib.EzModLib;

import net.minecraft.block.BlockState;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.IItemTier;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.PickaxeItem;
import net.minecraft.network.play.client.CPlayerDiggingPacket.Action;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.ToolType;

@SuppressWarnings("deprecation")
public class Hammer extends PickaxeItem {

	public String enumName;
	private String descriptionName;

	public Hammer(IItemTier material) {
		this(material, new Item.Properties());
	}

	public Hammer(IItemTier material, Item.Properties prop) {
		super(material, 1, -3.5f, prop.tab(ItemGroup.TAB_TOOLS));
		EzModLib.register(Main.MODID, (enumName = ((Enum<?>) material).name().toLowerCase()) + "_hammer", this);
	}

	@Override
	public float getDestroySpeed(ItemStack item, BlockState block) {
		return super.getDestroySpeed(item, block) / 2.8f;
	}

	@Override
	public boolean mineBlock(ItemStack hammer, World world, BlockState brokenBlock, BlockPos position, LivingEntity entityBreaking) {
		super.mineBlock(hammer, world, brokenBlock, position, entityBreaking);
		if (brokenBlock.getHarvestTool() != ToolType.PICKAXE || !(entityBreaking instanceof ServerPlayerEntity)) {
			return true;
		}
		ServerPlayerEntity player = (ServerPlayerEntity) entityBreaking;
		if (player.isCrouching()) {
			return true;
		}

		int side = 0;
		for (Entry<PlayerEntity, Pair<Direction, BlockPos>> g : Main.clickedBlockFaces.entrySet()) {
			if (g.getKey().equals(player)) {
				Pair<Direction, BlockPos> val = g.getValue();
				if (val.getSecond().equals(position)) {
					side = val.getFirst().get3DDataValue();
				} else {
					return true;
				}
				break;
			}
		}
		BlockState toBreak;
		int X = 0;
		int Y = 0;
		int Z = 0;

		if (side < 2) {
			X = 1;
			Z = 1;
		} else if (side > 3) {
			Y = 1;
			Z = 1;
		} else {
			Y = 1;
			X = 1;
		}

		int size = 3;

		int min = -(int) Math.floor(size / 2D);
		int max = (int) Math.ceil(size / 2D);
		int durability = hammer.getDamageValue();

		for (int m = min; m < max; m++) {
			for (int n = min; n < max; n++) {
				int x = position.getX() + (m * X);
				int y = position.getY() + (n * Y);
				int z = position.getZ() + ((Y == 0 ? n : m) * Z);
				BlockPos blockPos = new BlockPos(x, y, z);
				toBreak = world.getBlockState(blockPos);
				if (toBreak != null && toBreak.getHarvestTool() == ToolType.PICKAXE) {
					if (!blockPos.equals(position)) {
						player.gameMode.destroyAndAck(blockPos, Action.STOP_DESTROY_BLOCK, "destroyed");
					}
				}
			}
		}
		hammer.setDamageValue(durability);
		return true;
	}

	@Override
	public String getDescriptionId() {
		return descriptionName == null ? descriptionName = WordUtils.capitalize(this.getRegistryName().getPath().replace('_', ' ')) : descriptionName;
	}
}
